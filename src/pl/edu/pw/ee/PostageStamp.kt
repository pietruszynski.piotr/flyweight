package pl.edu.pw.ee

import java.time.Year

data class PostageStamp(
    val type: PostageStampType,
    val year: Year
)