package pl.edu.pw.ee

import java.time.Year

class Cluster {

    val stamps: MutableCollection<PostageStamp> = ArrayList()

    fun addStamp(
        x: Double,
        y: Double,
        name: String,
        year: Year
    ) {
        val type = PostageStampFactory.getPostageStampType(x, y, name)
        stamps.add(PostageStamp(type, year))
    }

}