package pl.edu.pw.ee

class PostageStampFactory {

    companion object {

        private val types: MutableMap<String, PostageStampType> = mutableMapOf()

        fun getPostageStampType(
            x: Double,
            y: Double,
            name: String
        ): PostageStampType {
            var stamp = types[name]
            if (stamp == null) {
                stamp = PostageStampType(x, y, name)
                types[name] = stamp
            }
            return stamp
        }

    }
}
