package pl.edu.pw.ee

import java.time.Year

fun main() {
    val cluster = Cluster()
    cluster.addStamp(3.0, 2.0, "Królowa Elżbieta", Year.parse("1993"))
    cluster.addStamp(3.0, 2.0, "Królowa Elżbieta", Year.parse("1994"))

    cluster.addStamp(21.37, 21.37, "Papiesz Polak", Year.parse("2001"))
    cluster.addStamp(21.37, 21.37, "Papiesz Polak", Year.parse("2002"))

    cluster.addStamp(10.0, 10.0, "Wieże Eiffla", Year.parse("1993"))
    cluster.addStamp(10.0, 10.0, "Wieże Eiffla", Year.parse("2000"))
    cluster.addStamp(10.0, 10.0, "Wieża Eiffla", Year.parse("2021"))

    val stamps = cluster.stamps
    stamps.forEach {
        println(it.toString())
    }

}