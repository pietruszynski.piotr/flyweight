package pl.edu.pw.ee

data class PostageStampType(
    val x: Double,
    val y: Double,
    val name: String
)